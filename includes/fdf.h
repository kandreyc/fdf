/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/18 20:15:10 by kandreyc          #+#    #+#             */
/*   Updated: 2017/01/26 18:21:41 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <stdio.h>
# include <fcntl.h>
# include <math.h>
# include <stdlib.h>
# include <unistd.h>
# include "./minilibx/mlx.h"
# include "getnextline/get_next_line.h"
# include "libft/libft.h"

typedef	struct	s_axis
{
	int		x;
	int		y;
	int		z;
	int		color;
}				t_axis;

typedef struct	s_session
{
	void	*mlx;
	void	*win;
	void	*img;
	char	*data;
	int		sizeline;
	int		end;
	int		bbp;
	t_axis	**xyz;

}				t_session;

void	line(double min_x, double min_y, double max_x, double max_y, void *mlx, char *data, int sizeline);
void    pixel_to_image(unsigned long img_color, char *data, int sizeline, int bpp, int x, int y);

#endif
