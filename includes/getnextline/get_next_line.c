/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/13 12:42:54 by kandreyc          #+#    #+#             */
/*   Updated: 2017/01/16 16:12:30 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static	t_line	*new(int fd)
{
	t_line			*elem;

	if ((elem = (t_line *)malloc(sizeof(t_line))) == NULL)
		return (NULL);
	elem->fd = fd;
	elem->buff = ft_memalloc(BUFF_SIZE + 1);
	elem->res = -1;
	elem->len = 0;
	elem->call = 0;
	elem->next_fd = NULL;
	return (elem);
}

static	t_line	*get_fd(t_line **fd_list, int fd)
{
	t_line			*cur;

	cur = NULL;
	if ((*fd_list) == NULL)
	{
		(*fd_list) = new(fd);
		cur = (*fd_list);
	}
	else
		(cur = (*fd_list));
	while (cur != NULL && cur->fd != fd)
		cur = cur->next_fd;
	if (cur == NULL)
	{
		((*fd_list)->next_fd != NULL) ? cur = (*fd_list)->next_fd : 0;
		(*fd_list)->next_fd = new(fd);
		(cur != NULL) ? (*fd_list)->next_fd->next_fd = cur : 0;
		cur = (*fd_list)->next_fd;
	}
	return (cur);
}

static	int		finder(t_line **fd, char *temp, char *temp2)
{
	while (1)
	{
		if ((*fd)->call > 0 && (*fd)->res != 0)
		{
			temp = ft_memalloc(BUFF_SIZE + 1);
			if (((*fd)->res = read((*fd)->fd, temp, BUFF_SIZE)) == -1)
				return (-1);
			temp2 = ft_strjoin((*fd)->buff, temp);
			free((*fd)->buff);
			free(temp);
			(*fd)->buff = temp2;
		}
		else if (((*fd)->res = read((*fd)->fd, (*fd)->buff, BUFF_SIZE)) == -1)
			return (-1);
		if ((*fd)->res == 0)
			return (0);
		if (ft_strchr((*fd)->buff, 10) != NULL)
		{
			(*fd)->len = ft_strlen((*fd)->buff);
			return (1);
		}
		else if ((*fd)->len == ft_strlen((*fd)->buff) && (*fd)->res == 0)
			return (0);
	}
	return (1);
}

static	void	get_next_ready(t_line **cur, char **line)
{
	int		i;
	char	*temp;

	i = 0;
	temp = NULL;
	while ((*cur)->buff[i] != '\n' && (*cur)->buff[i] != '\0')
		i++;
	*line = ft_strsub((*cur)->buff, 0, i);
	if ((*cur)->buff[i] == '\n')
	{
		temp = ft_strsub((*cur)->buff, (i + 1), (*cur)->len);
		free((*cur)->buff);
		(*cur)->buff = temp;
	}
	else
	{
		free((*cur)->buff);
		(*cur)->buff = ft_memalloc(BUFF_SIZE + 1);
	}
}

int				get_next_line(const int fd, char **line)
{
	static	t_line	*fd_list;
	t_line			*cur;

	cur = get_fd(&fd_list, fd);
	if (cur->res != 0 && ft_strchr(cur->buff, 10) == '\0')
	{
		cur->call += 1;
		if (finder(&cur, NULL, NULL) == -1)
			return (-1);
	}
	if (cur->res == 0 && cur->buff[0] == '\0')
	{
		(*line) = ft_memalloc(1);
		return (0);
	}
	get_next_ready(&cur, line);
	return (1);
}
