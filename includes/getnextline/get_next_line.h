/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/13 14:37:15 by kandreyc          #+#    #+#             */
/*   Updated: 2017/01/18 20:15:08 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>
# include <stdlib.h>
# include "../libft/libft.h"

# define BUFF_SIZE 1

typedef	struct		s_line
{
	int				fd;
	char			*buff;
	int				res;
	size_t			len;
	int				call;
	struct s_line	*next_fd;
}					t_line;

int					get_next_line(const int fd, char **line);

#endif
