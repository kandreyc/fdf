/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 13:53:31 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:56:57 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	unsigned int	j;
	char			*new;

	j = -1;
	if (s == NULL)
		return (NULL);
	new = ft_memalloc(ft_strlen(s) + 1);
	if (!new)
		return (NULL);
	new = ft_strcpy(new, s);
	while (new[++j])
		new[j] = f(new[j]);
	return (new);
}
