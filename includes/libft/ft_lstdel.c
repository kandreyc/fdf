/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/05 12:29:16 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:54:44 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void*, size_t))
{
	t_list	*buf;

	while (*alst)
	{
		buf = *alst;
		(*alst) = (*alst)->next;
		del(buf->content, buf->content_size);
		free(buf);
		(buf) = NULL;
	}
}
