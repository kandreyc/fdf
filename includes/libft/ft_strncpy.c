/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 16:16:03 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:57:13 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

char	*ft_strncpy(char *dst, const char *str, size_t len)
{
	size_t	k;

	k = -1;
	while (str[++k] && k < len)
		dst[k] = str[k];
	if (len > ft_strlen(str))
		while (k < len)
			dst[k++] = '\0';
	return (dst);
}
