/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 20:33:03 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/12 16:17:43 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	i;
	char	*l;
	int		j;

	j = ft_strlen(s) - 1;
	i = c;
	l = (char*)s;
	if (s[0] == '\0')
		return (NULL);
	if (c == '\0')
		return (l + ft_strlen(s));
	while (j >= 0)
	{
		if (s[j] == i)
			return (l + j);
		else
			j--;
	}
	return (0);
}
