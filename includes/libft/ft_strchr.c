/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 19:24:41 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:56:05 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

char	*ft_strchr(const char *s, int c)
{
	int		k;
	char	i;
	char	*l;

	i = c;
	k = 0;
	l = (char*)s;
	if (s[0] == '\0')
		return (NULL);
	if (c == '\0')
		return (l + ft_strlen(s));
	while (s[k])
	{
		if (s[k] == i)
			return (l + k);
		else
			k++;
	}
	return (0);
}
