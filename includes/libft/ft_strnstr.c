/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 14:56:06 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 18:25:23 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	k;
	size_t	j;

	k = -1;
	if (ft_strlen(little) > ft_strlen(big))
		return (NULL);
	else if (little[0] == '\0')
		return ((char*)big);
	while (big[++k] && k < len)
	{
		j = 0;
		if (big[k] == little[0] && k < len)
		{
			while (little[j] != '\0' && k < len)
				if (!(big[k++] == little[j++]))
					break ;
			if (little[j] == '\0' && big[k - 1] == little[j - 1])
				return ((char*)big + (k - j));
		}
		k = k - j;
	}
	return (NULL);
}
