/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/12 15:50:03 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/13 19:44:58 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_realloc(void *str, size_t newsize)
{
	size_t	size;
	void	*newstr;

	size = 0;
	if (str == NULL)
		return (NULL);
	newstr = malloc(newsize);
	if (newstr == NULL)
		return (NULL);
	size = ft_strlen(str);
	if (newsize < size)
		size = newsize;
	newstr = ft_memcpy(newstr, str, size);
	free(str);
	return (newstr);
}
