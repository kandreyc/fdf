/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 14:04:31 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:57:04 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	size_t	k;
	size_t	j;

	j = 0;
	k = ft_strlen(s1);
	while (s2[j] && j < n)
		s1[k++] = s2[j++];
	s1[k] = '\0';
	return (s1);
}
