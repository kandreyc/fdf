/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 14:47:06 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:57:00 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	j;
	char			*new;

	j = -1;
	if (!s)
		return (NULL);
	new = ft_memalloc(ft_strlen(s) + 1);
	if (!new)
		return (NULL);
	new = ft_strcpy(new, s);
	while (new[++j])
		new[j] = f(j, new[j]);
	return (new);
}
