/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpushparams.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/13 17:30:11 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/13 18:33:53 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstpushparams(int ac, char **av)
{
	int		i;
	t_list	*head;
	t_list	*lst;

	i = 2;
	if (ac < 2)
		return (NULL);
	lst = ft_lstnew(av[1], ft_strlen(av[1]));
	head = lst;
	while (i < ac)
	{
		lst->next = ft_lstnew(av[i], ft_strlen(av[i]));
		lst = lst->next;
		i++;
	}
	lst->next = NULL;
	return (head);
}
