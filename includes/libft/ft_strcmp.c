/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 13:01:16 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:56:13 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

int		ft_strcmp(const char *s1, const char *s2)
{
	int		k;

	k = -1;
	while (s1[++k])
		if (s1[k] != s2[k])
			break ;
	return ((unsigned char)s1[k] - (unsigned char)s2[k]);
}
