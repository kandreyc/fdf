/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 14:08:12 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:56:48 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	j;
	size_t	k;

	k = 0;
	while (dst[k] && k < size)
		k++;
	j = 0;
	if (size == k)
		return (k + ft_strlen(src));
	size = size - k - 1;
	while (j < size && src[j] != '\0')
	{
		dst[k + j] = src[j];
		j++;
	}
	dst[k + j] = '\0';
	return (k + ft_strlen(src));
}
