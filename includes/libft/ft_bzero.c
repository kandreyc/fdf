/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 18:28:04 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/16 14:27:42 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

void	*ft_bzero(void *s, size_t n)
{
	char			*k;
	size_t			j;

	j = 0;
	k = (char*)s;
	if (sizeof(n) == 0)
		return (k);
	while (j < n)
		k[j++] = 0;
	return (k);
}
