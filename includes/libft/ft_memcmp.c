/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 13:42:19 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:55:12 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t			k;
	unsigned char	*m1;
	unsigned char	*m2;

	m1 = (unsigned char*)s1;
	m2 = (unsigned char*)s2;
	k = -1;
	while (++k < n)
		if (m1[k] != m2[k])
			return (m1[k] - m2[k]);
	return (0);
}
