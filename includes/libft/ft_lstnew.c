/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/05 11:36:24 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/09 11:27:02 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*elem;

	elem = (t_list *)malloc(sizeof(t_list));
	if (!elem)
		return (NULL);
	if (!content)
	{
		elem->content = (void*)NULL;
		elem->content_size = 0;
	}
	else
	{
		elem->content = (t_list *)malloc(content_size);
		elem->content_size = content_size;
		elem->content = ft_memcpy(elem->content,
						(void *)content, elem->content_size);
		elem->next = NULL;
	}
	return (elem);
}
