/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 19:09:01 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:54:23 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

int		ft_isalpha(int k)
{
	if ((k >= 65 && k <= 90) || (k >= 97 && k <= 122))
		return (1);
	return (0);
}
