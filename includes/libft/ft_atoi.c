/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 19:58:14 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/17 12:53:44 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

int		ft_atoi(char *str)
{
	size_t		k;
	int			num;
	int			neg;

	k = 0;
	num = 0;
	neg = 1;
	while (str[k] == ' ' || str[k] == '\n' || str[k] == '\t'
		|| str[k] == '\r' || str[k] == '\v' || str[k] == '\f')
		k++;
	if ((str[k] == '-' && str[k + 1] >= '0' && str[k + 1] <= '9') ||
	(str[k] == '+' && str[k + 1] >= '0' && str[k + 1] <= '9'))
	{
		if (str[k] == '-')
			neg = -1;
		k++;
	}
	while (str[k] >= '0' && str[k] <= '9')
	{
		num *= 10;
		num += ((int)str[k] - 48);
		k++;
	}
	return (num * neg);
}
