/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsearch.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/12 17:45:34 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/12 22:04:40 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstsearch(t_list *lst, const char *s)
{
	if (lst == NULL)
		return (NULL);
	else
		while (lst)
		{
			if (ft_strcmp(lst->content, s) == 0)
				return (lst);
			lst = lst->next;
		}
	return (NULL);
}
