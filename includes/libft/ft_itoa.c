/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 15:25:54 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/15 11:14:49 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

static int		ft_intlen(long int n)
{
	int		len;

	len = 0;
	while (n > 0)
	{
		n = n / 10;
		len++;
	}
	return (len);
}

static char		*gg(char *mass, int k, long int l, int neg)
{
	if (l > 9)
	{
		mass[k--] = (l % 10) + 48;
		l = l / 10;
		gg(mass, k, l, neg);
	}
	else if (l < 10)
		mass[k--] = (char)l + 48;
	return (mass);
}

char			*ft_itoa(int n)
{
	char		*mass;
	long int	l;
	int			neg;
	int			k;

	k = 0;
	neg = 0;
	l = n;
	if (l <= 0)
	{
		neg = 1;
		l *= -1;
		k += 1;
	}
	k = k + ft_intlen(l);
	mass = (char*)malloc(sizeof(char) * (k + 1));
	if (mass == NULL)
		return (NULL);
	mass[k--] = '\0';
	mass = gg(mass, k, l, neg);
	if (neg == 1 && n != 0)
		mass[0] = '-';
	return (mass);
}
