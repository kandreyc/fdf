/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 13:09:00 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 18:55:42 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t			k;
	unsigned char	*src;

	k = 0;
	src = (unsigned char*)s;
	k = -1;
	while (++k < n)
		if (src[k] == (unsigned char)c)
			return (src + k);
	return (NULL);
}
