/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 17:56:09 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/06 15:58:44 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	k;

	k = 0;
	if (s1 != NULL && s2 != NULL)
		if (n > 0 && s2[0] != s1[0])
			return ((unsigned char)s1[0] - (unsigned char)s2[0]);
	while (k + 1 < n && s1[k] != '\0')
	{
		if (s1[k + 1] != s2[k + 1])
			return ((unsigned char)s1[k + 1] - (unsigned char)s2[k + 1]);
		k++;
	}
	return (0);
}
