/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 18:45:34 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:55:04 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char	l;
	unsigned char	*k;
	unsigned char	*d;
	size_t			j;

	j = -1;
	l = (unsigned char)c;
	k = (unsigned char*)dst;
	d = (unsigned char*)src;
	while (++j < n)
	{
		k[j] = d[j];
		if (d[j] == l)
			return (k + (j + 1));
	}
	return (NULL);
}
