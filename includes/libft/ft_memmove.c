/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 20:03:53 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/14 17:17:17 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char	*d;
	char	*s;
	size_t	k;

	k = -1;
	d = (char*)dst;
	s = (char*)src;
	if (s < d)
	{
		k = len - 1;
		while (len > 0)
		{
			d[k] = s[k];
			k--;
			len--;
		}
	}
	else
		while (++k < len)
			d[k] = s[k];
	return (d);
}
