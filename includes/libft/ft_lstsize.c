/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsize.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/13 16:39:25 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/13 19:04:52 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_lstsize(t_list *begin_list)
{
	int		i;
	t_list	*temp;

	i = 0;
	if (begin_list != NULL)
	{
		temp = begin_list;
		while (temp)
		{
			temp = temp->next;
			i++;
		}
	}
	return (i);
}
