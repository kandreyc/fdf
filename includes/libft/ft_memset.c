/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 17:50:38 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:55:26 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	l;
	char			*k;
	size_t			j;

	j = 0;
	l = (unsigned char)c;
	k = (char*)b;
	while (j < len)
		k[j++] = l;
	return (k);
}
