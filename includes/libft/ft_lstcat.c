/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/13 19:23:19 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/14 18:08:39 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstcat(t_list *begin_list1, t_list *begin_list2)
{
	t_list	*lst;

	if (begin_list1 != NULL && begin_list2 != NULL)
	{
		lst = begin_list1;
		while (lst->next)
			lst = lst->next;
		lst->next = begin_list2;
	}
}
