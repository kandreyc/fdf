/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 21:44:44 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:55:54 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

void	ft_putstr(char const *str)
{
	int		k;

	k = 0;
	if (str != NULL)
		while (str[k])
			ft_putchar(str[k++]);
}
