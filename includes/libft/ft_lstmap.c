/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/05 13:38:34 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/06 15:26:59 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new;
	t_list	*ret;

	new = NULL;
	if (!lst)
		return (NULL);
	else
	{
		new = (t_list*)malloc(sizeof(t_list));
		if (!new)
			return (0);
		new = f(lst);
		ret = new;
		while (lst->next)
		{
			lst = lst->next;
			new->next = f(lst);
			if (!new)
				return (NULL);
			new = new->next;
		}
	}
	return (ret);
}
