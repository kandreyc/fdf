/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 17:14:18 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:55:51 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	long int	d;

	d = (long int)n;
	if (d < 0)
	{
		ft_putchar_fd('-', fd);
		d *= -1;
	}
	if (d > 9)
	{
		ft_putnbr_fd(d / 10, fd);
		ft_putnbr_fd(d % 10, fd);
	}
	else
		ft_putchar_fd(d + 48, fd);
}
