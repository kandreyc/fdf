/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 22:15:07 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/14 13:40:35 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

void	*ft_memalloc(size_t size)
{
	size_t	k;
	char	*dst;

	k = 0;
	dst = (char *)malloc(sizeof(char) * size);
	if (!dst)
		return (NULL);
	while (k < size)
		dst[k++] = '\0';
	return (dst);
}
