/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 20:32:00 by kandreyc          #+#    #+#             */
/*   Updated: 2016/12/05 14:57:44 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

char	*ft_strstr(const char *big, const char *little)
{
	int		k;
	int		j;
	int		c;

	k = -1;
	if (ft_strlen(little) > ft_strlen(big))
		return (NULL);
	else if (little[0] == '\0')
		return ((char*)big);
	while (big[++k])
	{
		c = k;
		j = 0;
		if (big[k] == little[0])
			while (little[j])
				if (!(big[k++] == little[j++]))
					break ;
		if (big[k - 1] == little[j - 1] && j != 0)
			return ((char*)big + c);
		k = c;
	}
	return (NULL);
}
