void    my_pixel_put_to_image(unsigned long img_color, char *data, int sizeline, int bpp, int x, int y)
{
	unsigned char r;
	unsigned char g;
	unsigned char b;

	b = ((img_color & 0xFF));
	g = ((img_color & 0xFF00) >> 8);
	r = ((img_color & 0xFF0000) >> 16);

	data[y * sizeline + x * 4] = b;
	data[y * sizeline + x * 4 + 1] = g;
	data[y * sizeline + x * 4 + 2] = r;
}

help	explain(bit manipulation)
{
	sample unsigned long in binary - 00000000111111110000000000000000;

	0xFF		is 11111111;
	0xFF0000	is 111111110000000000000000;
	0xFF00		is 1111111100000000;

	symbol & - знак обьединения, который оставляет 1, если она находится в двух числах, которые обьединяются.
	{
		1 + 1 = 1;
		1 + 0 = 0;
		0 + 0 = 0.
	}

	symbol | - знак обьединения, который оставляет 1, если она находится в двух числах, которые обьединяются, и если в одном из чисел есть 1.
	{
		1 + 1 = 1;
		1 + 0 = 1;
		0 + 0 = 0;
	}
	after unification 00000000000000000000000011111111 and 00000000111111110000000000000000 result is 00000000000000000000000000000000

	after unification 00000000000000001111111100000000 and 00000000111111110000000000000000 result is 00000000000000000000000000000000

	after unification 00000000111111110000000000000000 and 00000000111111110000000000000000 result is 				   000000011111111
}
