/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/26 15:36:32 by kandreyc          #+#    #+#             */
/*   Updated: 2017/01/26 17:24:17 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <math.h>

int		hex_to_dec(char *str);
{
	int		i;
	int		j;
	long	num;

	num = 0;
	i = -1;
	j = 6;
	while (++i < 6)
	{
		if (str[i] > 47 && str[i] < 58)
			num += ((str[i] - 48) * pow(16, --j));
		else if (str[i] > 64 && str[i] < 71)
			num += ((str[i] - 55) * pow(16, --j));
		else if (str[i] > 96 && str[i] < 103)
			num += ((str[i] - 87) * pow(16, --j));
		else
			error(1);
	}
	return (num);
}
