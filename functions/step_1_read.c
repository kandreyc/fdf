/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   step_1_read.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/18 18:10:15 by kandreyc          #+#    #+#             */
/*   Updated: 2017/01/26 18:21:40 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

int		hex_to_dec(char *str);
{
	int		i;
	int		j;
	long	num;

	num = 0;
	i = -1;
	j = 6;
	while (++i < 6)
	{
		if (str[i] > 47 && str[i] < 58)
			num += ((str[i] - 48) * pow(16, --j));
		else if (str[i] > 64 && str[i] < 71)
			num += ((str[i] - 55) * pow(16, --j));
		else if (str[i] > 96 && str[i] < 103)
			num += ((str[i] - 87) * pow(16, --j));
		else
			error(1);
	}
	return (num);
}

t_axis		new_axis_array(int *coordinates, int width, int height)
{
	t_axis	**xyz;
	int		x;
	int		y;
	int		i;
	int		j;

	y = 0;
	i = 0;
	j = 0;
	xyz = (t_axis **)malloc(sizeof(t_axis *) * (height + 1));
	while (y < height)
	{
		x = 0;
		while (x < width)
		{
			////////////////////////////////////////////////////////////////////////////
		}
	}
	xyz[height] = 0;
	return (xyz);
}

t_session	new_session(int width, int height, char *filename)
{
	int			i;
	t_axis		**xyz;
	char		*data;
	t_session	*session;

	i = 0;
	session = (t_session *)malloc(sizeof(t_session));
	session->end = 0;
	session->bbp = 32;
	session->sizeline = width * 4;
	session->mlx = mlx_init();
	session->win = mlx_new_window(session->mlx, width + 100, height + 100, filename);
	session->img = mlx_new_image(session->mlx, width, height);
	session->data = mlx_get_data_addr(session->img, &(session->bbp), &(session->sizeline), &(session->end));
	session->xyz = new_axis(xyz_len, coordinates, 0, 0);
	return (session);
}

void	read_it(int ac, char **av)
{
	int		fd;

	fd = open(s);
	if ()
}


int		main(void)
{
	// line(1, 2, 10, 2, mlx, data, sizeline);
	// line(100, 100, 100, 200, mlx, data, sizeline);
	// line(100, 100, 200, 100, mlx, data, sizeline);
	// line(100, 200, 200, 200, mlx, data, sizeline);
	// line(200, 100, 200, 200, mlx, data, sizeline);
	// line(500, 500, 600, 600, mlx, data, sizeline);
	// line(300, 300, 400, 400, mlx, data, sizeline);
	// line(700, 700, 700, 700, mlx, data, sizeline);
	// line(400, 400, 600, 450, mlx, data, sizeline);
	// line(400, 400, 410, 420, mlx, data, sizeline);
	line(100, 100, 200, 125, mlx, data, sizeline);
	// line(0, 0, 500, 500, mlx, data, sizeline);
	// line(200, 100, 350, 220, mlx, win);
	mlx_put_image_to_window(mlx, win, img, 0, 0);
	// line (450, 250, 50, 100, mlx, win);
	// color = mlx_get_data_addr(img, &t1, &t2, &end);
	// mlx_put_image_to_window(mlx, win, img, 0, 0);
	mlx_loop(mlx);
	return (0);
}
