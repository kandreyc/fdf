/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kandreyc <kandreyc@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 12:07:52 by kandreyc          #+#    #+#             */
/*   Updated: 2017/01/26 18:21:38 by kandreyc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

#define EXP_X fabs((max_x - min_x)) / fabs((max_y - min_y)) * (y - min_y) + min_x
#define EXP_Y fabs((max_y - min_y)) / fabs((max_x - min_x))

void	pixel_to_image(unsigned long img_color, char *data, int sizeline, int bpp, int x, int y)
{
	unsigned char r;
	unsigned char g;
	unsigned char b;

	b = ((img_color & 0xFF));
	g = ((img_color & 0xFF00) >> 8);
	r = ((img_color & 0xFF0000) >> 16);
	data[y * sizeline + x * 4] = b;
	data[y * sizeline + x * 4 + 1] = g;
	data[y * sizeline + x * 4 + 2] = r;
}

void	swap(double *a, double *b, double *c, double *d)
{
	double	tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
	tmp = *c;
	*c = *d;
	*d = tmp;
}

void	line(double min_x, double min_y, double max_x, double max_y, void *mlx, char *data, int sizeline)
{
	double	x;
	double	y;
	double	s;

	(min_x > max_x && min_y > max_y) ? swap(&min_x, &max_x, &min_y, &max_y) : 0;
	x = min_x;
	y = min_y;
	if (min_y != max_y)
		s = 0;
	else
		s = EXP_Y;
	while (min_y != max_y || min_x != max_x)
	{
		(min_y != max_y) ? (s = EXP_X) : (y += s);
		(s != 0 && (s - x) < 2) ? x = s : 0;
		pixel_to_image(0x2E8B57, data, sizeline, 32, x, y);
		if (s - x > 2)
			x++;
		else
			(min_y != max_y) ? y++ : x++;
		if ((y == max_y + 1 && x >= max_x) || (x == max_x + 1 && y >= max_y))
			return ;
	}
	if (min_y == max_y && min_x == max_x)
		pixel_to_image(0x2E8B57, data, sizeline, 32, x, y);
}
