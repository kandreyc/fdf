#[----PROJECT NAME----]#
NAME		= fdf


#[----PATH TO HEADERS----]#
FDF_H		= ./includes/
LIBFT_H 	= ./includes/libft/
GNL_H		= ./includes/getnextline/
MLX_H		= ./includes/minilibx/


#[---COMPILE---]#
ACT			= gcc
FLAGS		= -Wall -Wextra -Werror


#[----ATTACH THE HEADERS----]#
GCC_MLX_H	= -I libmlx_intel-mac -L $(MLX_H) -lmlx -framework OpenGL -framework AppKit
GCC_LFT_H	= -I libft -L $(LIBFT_H) -lft
GCC_GNL_H	= -I $(GNL_H)
GCC_FDF_H	= -I $(FDF_H)
GCC_ALL_H	= $(GCC_MLX_H) $(GCC_LFT_H) $(GCC_GNL_H) $(GCC_FDF_H)

#[----LFT SOURCES----]#
SRC_LFT_C		= ./includes/libft/*.c
SRC_LFT_O		= ./includes/libft/*.o

#[----FDF SOURCES----]#
SRC_FDF_C		= ./functions/step_1_read.c ./functions/line.c
SRC_FDF_O		= ./functions/step_1_read.o ./functions/line.o


#[----GNL SOURCES]----#
SRC_GNL_C		= ./includes/getnextline/get_next_line.c
SRC_GNL_O		= ./includes/getnextline/get_next_line.o


#[----ALL SOURCES]----#
TEMP_O			= temp/*.o
SRC_O			= $(SRC_GNL_O) $(SRC_FDF_O)

#[----COLORS]----#
GREEN		= \033[32m
DEFAULT		= \033[39m


all: $(NAME)

$(NAME): $(SRC_O)
	@ make -C $(LIBFT_H)
	@ mkdir temp
	@ mv $(SRC_O) temp/
	@ $(ACT) $(FLAGS) $(GCC_ALL_H) $(TEMP_O) -o $(NAME)
	@ echo "$(GREEN)[fdf]$(DEFAULT)"

%.o : ./functions/%.c $(FDF_H)
	@ $(ACT) $(FLAGS) $(GCC_ALL_H) -c -o $@ $<

norm:
	norminette -R CheckForbiddenSourceHeader $(SRC_FDF_C) $(FDF_H)fdf.h
	@ echo "$(GREEN)[norminette]"

clean:
	@ rm -rf temp
	@ make clean -C $(LIBFT_H)
	@ echo "$(GREEN)[clean]$(DEFAULT)"

fclean: clean
	@ rm -f $(NAME)
	@ make fclean -C $(LIBFT_H)
	@ echo "$(GREEN)[fclean]$(DEFAULT)"

re: fclean all